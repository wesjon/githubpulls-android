package com.wesjon.githubpulls.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import com.wesjon.githubpulls.R
import com.wesjon.githubpulls.util.bindOptionalView

/**
 * Created by 1545 IRON V4 on 9/21/2017.
 */
abstract class BaseActivity : AppCompatActivity() {
    abstract val layoutRes: Int

    private val toolbar by bindOptionalView<Toolbar>(R.id.toolbar)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(layoutRes)

        toolbar?.let {
            setSupportActionBar(it)

            supportActionBar?.let {
                it.setDisplayHomeAsUpEnabled(!isTaskRoot)
                it.setDisplayShowHomeEnabled(true)
            }

            it.setNavigationOnClickListener { onBackPressed() }
        }

        val extras = intent?.extras
        if (extras != null)
            loadExtras(extras)
    }

    open fun loadExtras(extras: Bundle){}
}