package com.wesjon.githubpulls.base

import android.arch.lifecycle.ViewModel

/**
 * Created by 1545 IRON V4 on 9/23/2017.
 */
abstract class BasePresenter<T : BaseView> : ViewModel() {
    var view: T? = null

    open fun attachView(view: T) {
        this.view = view
    }

    /*
    All ViewModels from architecture components must have a empty constructor therefore you must
    override this function to tell whether your presenter has been initialized or not
     */
    abstract fun isInitialized(): Boolean
}