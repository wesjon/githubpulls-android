package com.wesjon.githubpulls.util

import android.graphics.*
import com.squareup.picasso.Transformation


/**
 * Created by julian
 * GITHUB snippet: https://gist.github.com/julianshen/5829333
 */
class CircleTransform : Transformation {
    override fun transform(source: Bitmap): Bitmap {
        val size = Math.min(source.width, source.height)

        val x = (source.width - size) / 2
        val y = (source.height - size) / 2

        val squaredBitmap = Bitmap.createBitmap(source, x, y, size, size)

        val bitmap = Bitmap.createBitmap(size, size, source.config)
        val shaderClamp = BitmapShader(squaredBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)

        val paint = Paint().apply {
            shader = shaderClamp
            isAntiAlias = true
        }

        val r = size / 2f
        Canvas(bitmap).drawCircle(r, r, r, paint)

        source.recycle()
        squaredBitmap.recycle()
        return bitmap
    }

    override fun key(): String {
        return "circle"
    }
}