package com.wesjon.githubpulls.util

import android.os.Bundle

/**
 * Created by 1545 IRON V4 on 9/23/2017.
 */
fun Bundle.putKeyValue(key: String, value: Any?): Bundle {
    when (value) {
        is String -> putString(key, value)
        is Int -> putInt(key, value)
    }
    return this
}

fun <T> Bundle.getExtraForKey(key: String, required: Boolean = false): T {
    val value = get(key)
    if (required && value == null) {
        throw RuntimeException("This value is required")
    }

    return value as T
}