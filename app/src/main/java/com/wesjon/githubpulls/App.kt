package com.wesjon.githubpulls

import android.app.Application

/**
 * Created by 1545 IRON V4 on 9/21/2017.
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()

        instance = this
    }

    companion object {
        lateinit var instance: App
    }
}