package com.wesjon.githubpulls.rest.deserializer

import com.google.gson.*
import java.lang.reflect.Type

/**
 * Created by 1545 IRON V4 on 9/21/2017.
 */
class ListDeserializer<T> : JsonDeserializer<T> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): T {
        var root: JsonArray? = null

        when (json) {
            is JsonObject -> {
                var listElementName = "items"
                for ((key, value) in json.entrySet()) if (value.isJsonArray) {
                    listElementName = key
                    break
                }

                root = json.getAsJsonArray(listElementName)
            }
        }

        return Gson().fromJson<T>(root, typeOfT)
    }
}