package com.wesjon.githubpulls.rest

import android.util.Log
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.wesjon.githubpulls.App
import com.wesjon.githubpulls.BuildConfig
import com.wesjon.githubpulls.R
import com.wesjon.githubpulls.rest.deserializer.ListDeserializer
import com.wesjon.githubpulls.rest.model.Repository
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.lang.reflect.Type

/**
 * Created by 1545 IRON V4 on 9/21/2017.
 */
object RestClient {
    private inline fun <reified T> genericType(): Type = object : TypeToken<T>() {}.type

    val apiService: GithubApiService by lazy {
        val baseUrl = App.instance.getString(R.string.github_api_base_url)

        val okHttpClientBuilder = OkHttpClient.Builder()
        okHttpClientBuilder.cache(okHttpCache)
        if (BuildConfig.DEBUG) {
            okHttpClientBuilder
                    .addInterceptor(HttpLoggingInterceptor()
                            .setLevel(HttpLoggingInterceptor.Level.BODY))
        }

        val gson = GsonBuilder()
                .registerTypeAdapter(genericType<ArrayList<Repository>>(), ListDeserializer<ArrayList<Repository>>())
                .create()

        Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClientBuilder.build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(GithubApiService::class.java)
    }

    private val okHttpCache: Cache?
        get() {
            return try {
                Cache(File(App.instance.cacheDir, "http-cache"), (15 * 1024 * 1024).toLong())
            } catch (e: Exception) {
                Log.e("RestClient", "Could not create Cache!")
                null
            }
        }
}

