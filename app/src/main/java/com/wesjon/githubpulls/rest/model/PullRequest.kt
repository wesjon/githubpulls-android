package com.wesjon.githubpulls.rest.model

import com.google.gson.annotations.SerializedName

/**
 * Created by 1545 IRON V4 on 9/21/2017.
 */
data class PullRequest(
        val title: String,
        val body: String,
        val state: String,
        @SerializedName("html_url") val htmlUrl: String = "",
        @SerializedName("user") val owner: Owner
) {
    companion object {
        const val PR_STATE_OPEN = "open"
        const val PR_STATE_CLOSED = "closed"
    }
}