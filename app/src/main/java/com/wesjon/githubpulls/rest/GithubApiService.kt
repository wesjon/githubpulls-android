package com.wesjon.githubpulls.rest

import com.wesjon.githubpulls.rest.model.PullRequest
import com.wesjon.githubpulls.rest.model.Repository
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by 1545 IRON V4 on 9/21/2017.
 */
interface GithubApiService {
    @GET("search/repositories?q=language:Java&sort=stars&")
    fun getJavaRepositories(@Query("page") page: Int): Call<ArrayList<Repository>>

    @GET("repos/{ownerUsername}/{repoName}/pulls")
    fun getPullsFromRepo(@Path("ownerUsername") ownerName: String, @Path("repoName") repoName: String): Call<ArrayList<PullRequest>>
}