package com.wesjon.githubpulls.rest.model

import com.google.gson.annotations.SerializedName

/**
 * Created by 1545 IRON V4 on 9/21/2017.
 */
data class Repository(
        val name: String,
        val description: String?,
        @SerializedName("forks") val forksCount: Int,
        @SerializedName("stargazers_count") val starsCount: Int,
        val owner: Owner,
        val state: String
)