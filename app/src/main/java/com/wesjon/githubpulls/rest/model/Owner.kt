package com.wesjon.githubpulls.rest.model

import com.google.gson.annotations.SerializedName

/**
 * Created by 1545 IRON V4 on 9/21/2017.
 */
data class Owner(
        @SerializedName("avatar_url") val avatarUrl: String?,
        @SerializedName("login") val username: String
)