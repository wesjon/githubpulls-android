package com.wesjon.githubpulls.screens.pull_requests

import com.wesjon.githubpulls.base.BaseView
import com.wesjon.githubpulls.rest.model.PullRequest

/**
 * Created by 1545 IRON V4 on 9/23/2017.
 */
interface PullRequestListContract {
    interface View : BaseView {
        fun showLoading()
        fun hideLoading()
        fun setPrs(pullRequests: ArrayList<PullRequest>)
        fun showError(errorRes: Int)
        fun setPrsOpenClosedBalance(prsOpen: Int, prsClosed: Int)
    }
}