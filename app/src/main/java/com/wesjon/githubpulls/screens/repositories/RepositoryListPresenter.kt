package com.wesjon.githubpulls.screens.repositories

import android.support.annotation.VisibleForTesting
import com.wesjon.githubpulls.R
import com.wesjon.githubpulls.base.BasePresenter
import com.wesjon.githubpulls.rest.GithubApiService
import com.wesjon.githubpulls.rest.model.Repository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by 1545 IRON V4 on 9/23/2017.
 */
class RepositoryListPresenter : BasePresenter<RepositoryListContract.View>() {
    private var api: GithubApiService? = null
    private var repositories: ArrayList<Repository>? = null

    @VisibleForTesting
    var currentPage = FIRST_PAGE

    private var callFetchPage: Call<ArrayList<Repository>>? = null

    fun loadNextPage() {
        when (currentPage) {
            FIRST_PAGE -> view?.showLoadingScreen()
            else -> view?.showLoadingNextPage()
        }

        callFetchPage = api?.getJavaRepositories(currentPage)
        callFetchPage?.enqueue(object : Callback<ArrayList<Repository>> {
            override fun onFailure(call: Call<ArrayList<Repository>>?, t: Throwable?) {
                view?.hideLoadings()
                view?.showError(R.string.connection_error)
            }

            override fun onResponse(call: Call<ArrayList<Repository>>?, response: Response<ArrayList<Repository>>?) {
                view?.hideLoadings()
                if (response?.isSuccessful == true) {
                    currentPage++

                    val results = response.body() ?: ArrayList()
                    if (repositories == null) repositories = ArrayList()

                    repositories?.addAll(results)

                    setState()
                } else {
                    view?.showError(R.string.server_error_fetching_repos)
                }
            }
        })
    }

    private fun setState() {
        repositories?.let { view?.setItems(it) }
    }

    fun restorePage() {
        setState()
    }

    fun init(apiService: GithubApiService) {
        api = apiService
    }

    override fun onCleared() {
        super.onCleared()

        view = null
        callFetchPage?.cancel()
    }

    override fun isInitialized() = repositories != null

    companion object {
        val FIRST_PAGE = 1
    }
}