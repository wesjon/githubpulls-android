package com.wesjon.githubpulls.screens.repositories

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.wesjon.githubpulls.R
import com.wesjon.githubpulls.rest.model.Repository
import com.wesjon.githubpulls.util.Image
import com.wesjon.githubpulls.util.bindView
import org.greenrobot.eventbus.EventBus
import java.util.*

/**
 * Created by 1545 IRON V4 on 9/21/2017.
 */
internal class RepositoryListAdapter(val context: Context) : RecyclerView.Adapter<RepositoryListAdapter.ItemViewHolder>() {
    private var items = ArrayList<Repository>()

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ItemViewHolder {
        return ItemViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.item_repository_list, parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder?, position: Int) {
        holder?.bind(items[position])
    }

    override fun getItemCount() = items.size

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val repositoryName: TextView by bindView(R.id.repository_name)
        private val repositoryDescription: TextView by bindView(R.id.repository_description)
        private val forkCount: TextView by bindView(R.id.fork_count)
        private val starsCount: TextView by bindView(R.id.stars_count)
        private val authorImage: ImageView by bindView(R.id.author_image)
        private val authorUsername: TextView by bindView(R.id.author_username)
        private val parent = itemView

        init {
            parent.setOnClickListener { EventBus.getDefault().post(it.tag) }
        }

        fun bind(repository: Repository) {
            parent.tag = repository

            repositoryName.text = repository.name
            repositoryDescription.text = repository.description
            forkCount.text = repository.forksCount.toString()
            starsCount.text = repository.starsCount.toString()
            authorUsername.text = repository.owner.username

            Picasso.with(context)
                    .load(repository.owner.avatarUrl)
                    .transform(Image.circleTransformation)
                    .fit()
                    .placeholder(R.drawable.img_placeholder_40dp)
                    .into(authorImage)
        }
    }

    fun setItems(results: ArrayList<Repository>) {
        items = results
        notifyDataSetChanged()
    }
}