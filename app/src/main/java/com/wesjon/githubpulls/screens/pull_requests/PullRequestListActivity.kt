package com.wesjon.githubpulls.screens.pull_requests

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.constraint.Group
import android.support.v7.app.AlertDialog
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.wesjon.githubpulls.R
import com.wesjon.githubpulls.base.BaseActivity
import com.wesjon.githubpulls.rest.RestClient
import com.wesjon.githubpulls.rest.model.PullRequest
import com.wesjon.githubpulls.util.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

@SuppressLint("Registered")
/**
 * Created by 1545 IRON V4 on 9/21/2017.
 */
class PullRequestListActivity : BaseActivity(), PullRequestListContract.View {

    private lateinit var adapter: PullRequestListAdapter

    private val pullRequestList: RecyclerView by bindView(R.id.pull_request_list)
    private val prsBalanceText: TextView by bindView(R.id.open_closed_prs_balance)
    private val progress: View by bindView(R.id.progress)

    private lateinit var ownerUsername: String
    private lateinit var repoName: String

    override val layoutRes = R.layout.activity_pull_requests

    private lateinit var presenter: PullRequestListPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        title = repoName

        adapter = PullRequestListAdapter(this)

        pullRequestList.adapter = adapter
        pullRequestList.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        presenter = ViewModelProviders.of(this).get(PullRequestListPresenter::class.java)
        presenter.attachView(this)
        if (presenter.isInitialized()) {
            presenter.restorePrs()
        } else {
            presenter.init(RestClient.apiService, ownerUsername, repoName)
            presenter.loadPrs()
        }


    }

    override fun showLoading() {
        progress.beVisible()
    }

    override fun hideLoading() {
        progress.beGone()
    }

    override fun showError(errorRes: Int) {
        AlertDialog.Builder(this)
                .setMessage(errorRes)
                .setPositiveButton(R.string.try_again, { _, _ -> presenter.loadPrs() })
                .setNegativeButton(android.R.string.cancel, { _, _ -> onBackPressed() })
                .setCancelable(false)
                .show()
    }

    override fun setPrs(pullRequests: ArrayList<PullRequest>) {
        adapter.setItems(pullRequests)
    }

    override fun setPrsOpenClosedBalance(prsOpen: Int, prsClosed: Int) {
        prsBalanceText.text = getString(R.string.prs_balance, prsOpen, prsClosed)
    }

    override fun loadExtras(extras: Bundle) {
        ownerUsername = extras.getExtraForKey(KEY_REPO_USER, required = true)
        repoName = extras.getExtraForKey(KEY_REPO_NAME, required = true)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(pullRequest: PullRequest) {
        if (pullRequest.htmlUrl.isNotEmpty()) {
            startActivity(Intent(Intent.ACTION_VIEW).apply {
                data = Uri.parse(pullRequest.htmlUrl)
            })
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    companion object {
        private const val KEY_REPO_USER = "KEY_REPO_USER"
        private const val KEY_REPO_NAME = "KEY_REPO_NAME"

        fun getIntent(context: Context, ownerUsername: String, repoName: String): Intent {
            return Intent(context, PullRequestListActivity::class.java).apply {
                putExtras(Bundle()
                        .putKeyValue(KEY_REPO_USER, ownerUsername)
                        .putKeyValue(KEY_REPO_NAME, repoName))
            }
        }
    }
}