package com.wesjon.githubpulls.screens.pull_requests

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.wesjon.githubpulls.R
import com.wesjon.githubpulls.rest.model.PullRequest
import com.wesjon.githubpulls.util.Image
import com.wesjon.githubpulls.util.bindView
import org.greenrobot.eventbus.EventBus
import java.util.*

/**
 * Created by 1545 IRON V4 on 9/21/2017.
 */
class PullRequestListAdapter(val context: Context) : RecyclerView.Adapter<PullRequestListAdapter.ItemViewHolder>() {
    private var items = ArrayList<PullRequest>()

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ItemViewHolder {
        return ItemViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.item_pull_request_list, parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder?, position: Int) {
        holder?.bind(items[position])
    }

    override fun getItemCount() = items.size

    fun setItems(newItems: ArrayList<PullRequest>) {
        items = newItems
        notifyDataSetChanged()
    }

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val prTitle: TextView by bindView(R.id.pr_title)
        private val prDescription: TextView by bindView(R.id.pr_description)
        private val authorImage: ImageView by bindView(R.id.author_image)
        private val authorUsername: TextView by bindView(R.id.author_username)
        private val parent = itemView

        init {
            parent.setOnClickListener { EventBus.getDefault().post(it.tag) }
        }

        fun bind(pullRequest: PullRequest) {
            parent.tag = pullRequest

            prTitle.text = pullRequest.title
            prDescription.text = pullRequest.body
            authorUsername.text = pullRequest.owner.username

            Picasso.with(context)
                    .load(pullRequest.owner.avatarUrl)
                    .transform(Image.circleTransformation)
                    .fit()
                    .placeholder(R.drawable.img_placeholder_40dp)
                    .into(authorImage)
        }
    }
}