package com.wesjon.githubpulls.screens.repositories

import com.wesjon.githubpulls.base.BaseView
import com.wesjon.githubpulls.rest.model.Repository
import java.util.*

/**
 * Created by 1545 IRON V4 on 9/23/2017.
 */
interface RepositoryListContract {
    interface View : BaseView {
        fun setItems(results: ArrayList<Repository>)
        fun showLoadingNextPage()
        fun showLoadingScreen()
        fun showError(errorRes: Int)
        fun hideLoadings()
    }
}