package com.wesjon.githubpulls.screens.pull_requests

import com.wesjon.githubpulls.R
import com.wesjon.githubpulls.base.BasePresenter
import com.wesjon.githubpulls.rest.GithubApiService
import com.wesjon.githubpulls.rest.model.PullRequest
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by 1545 IRON V4 on 9/23/2017.
 */
class PullRequestListPresenter : BasePresenter<PullRequestListContract.View>() {
    private var api: GithubApiService? = null

    var pullRequests: ArrayList<PullRequest>? = null
    private lateinit var ownerUsername: String
    private lateinit var repoName: String

    private var callGetPr: Call<ArrayList<PullRequest>>? = null

    fun loadPrs() {
        view?.showLoading()

        callGetPr = api?.getPullsFromRepo(ownerUsername, repoName)
        callGetPr?.enqueue(object : Callback<ArrayList<PullRequest>> {
            override fun onFailure(call: Call<ArrayList<PullRequest>>?, t: Throwable?) {
                view?.hideLoading()
                view?.showError(R.string.connection_error)
            }

            override fun onResponse(call: Call<ArrayList<PullRequest>>?, response: Response<ArrayList<PullRequest>>?) {
                view?.hideLoading()
                if (response?.isSuccessful == true) {
                    pullRequests = response.body() ?: ArrayList()

                    setViewState()
                } else {
                    view?.showError(R.string.server_error_fetching_prs)
                }
            }
        })
    }

    private fun setViewState() {
        pullRequests?.let { prs ->
            val prsOpen = prs.count { it.state == PullRequest.PR_STATE_OPEN }
            val prsClosed = prs.count { it.state == PullRequest.PR_STATE_CLOSED }

            view?.setPrsOpenClosedBalance(prsOpen, prsClosed)
            view?.setPrs(prs)
        }
    }

    fun init(apiService: GithubApiService, ownerUsername: String, repoName: String) {
        api = apiService
        this.ownerUsername = ownerUsername
        this.repoName = repoName
    }

    fun restorePrs() {
        view?.hideLoading()

        setViewState()
    }

    override fun onCleared() {
        super.onCleared()

        view = null
        callGetPr?.cancel()
    }

    override fun isInitialized() = pullRequests != null
}