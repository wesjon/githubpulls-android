package com.wesjon.githubpulls.screens.repositories

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.wesjon.githubpulls.R
import com.wesjon.githubpulls.base.BaseActivity
import com.wesjon.githubpulls.rest.RestClient
import com.wesjon.githubpulls.rest.model.Repository
import com.wesjon.githubpulls.screens.pull_requests.PullRequestListActivity
import com.wesjon.githubpulls.util.EndlessRecyclerViewScrollListener
import com.wesjon.githubpulls.util.beGone
import com.wesjon.githubpulls.util.beVisible
import com.wesjon.githubpulls.util.bindView
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.util.*


class RepositoryListActivity : BaseActivity(), RepositoryListContract.View {
    override val layoutRes = R.layout.activity_repository_list

    private val repositoryList: RecyclerView by bindView(R.id.repository_list)
    private val loadingNextPage: View by bindView(R.id.loading_next_page)
    private val loadingScreen: View by bindView(R.id.loading_screen)

    private lateinit var adapter: RepositoryListAdapter
    private lateinit var presenter: RepositoryListPresenter
    private lateinit var scrollListener: EndlessRecyclerViewScrollListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        adapter = RepositoryListAdapter(this)

        repositoryList.adapter = adapter
        repositoryList.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        scrollListener = object : EndlessRecyclerViewScrollListener(repositoryList.layoutManager as LinearLayoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                presenter.loadNextPage()
            }
        }
        repositoryList.addOnScrollListener(scrollListener)

        presenter = ViewModelProviders.of(this).get(RepositoryListPresenter::class.java)
        presenter.attachView(this)
        if (presenter.isInitialized()) {
            presenter.restorePage()
        } else {
            presenter.init(RestClient.apiService)
            presenter.loadNextPage()
        }
    }

    override fun setItems(results: ArrayList<Repository>) {
        adapter.setItems(results)
        scrollListener.setState(results.size, false)
    }

    override fun showLoadingNextPage() {
        loadingNextPage.beVisible()
    }

    override fun showLoadingScreen() {
        loadingScreen.beVisible()
    }

    override fun hideLoadings() {
        loadingNextPage.beGone()
        loadingScreen.beGone()
    }

    override fun showError(errorRes: Int) {
        AlertDialog.Builder(this)
                .setMessage(errorRes)
                .setPositiveButton(R.string.try_again) { _, _ -> presenter.loadNextPage() }
                .setCancelable(false)
                .show()
    }

    @Subscribe
    fun onEvent(pullRequest: Repository) {
        startActivity(PullRequestListActivity.getIntent(this, pullRequest.owner.username, pullRequest.name))
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }
}
