package com.wesjon.githubpulls.screens.pull_requests

import com.wesjon.githubpulls.R
import com.wesjon.githubpulls.base.BasePresenterTest
import com.wesjon.githubpulls.base.FakeApiService
import com.wesjon.githubpulls.rest.model.Owner
import com.wesjon.githubpulls.rest.model.PullRequest
import okhttp3.ResponseBody
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import retrofit2.Call
import retrofit2.Response
import retrofit2.mock.Calls
import java.io.IOException

/**
 * Created by 1545 IRON V4 on 9/23/2017.
 */
class PullRequestListPresenterTest : BasePresenterTest() {
    @Mock
    lateinit var view: PullRequestListContract.View

    private val presenter = PullRequestListPresenter()

    private val ownerUsername = "joao"
    private val repoName = "Windows12"

    @Before
    fun setup() {
        presenter.attachView(view)
    }

    private fun getNPrs(prsOpenCount: Int, prsClosedCount: Int): ArrayList<PullRequest> {
        val prs = ArrayList<PullRequest>()
        val title = "pr_title"
        val body = "This is a pr to include super nice feature"
        val url = "xuxa.html"
        val owner = Owner("xuxa.jpg", ownerUsername)

        for (i in 0 until prsOpenCount) {
            prs.add(PullRequest(title, body, PullRequest.PR_STATE_OPEN, url, owner))
        }

        for (i in 0 until prsClosedCount) {
            prs.add(PullRequest(title, body, PullRequest.PR_STATE_CLOSED, url, owner))
        }

        return prs
    }

    @Test
    fun `load few pull requests successfully`() {
        val prsToReturn = getNPrs(3, 10)
        presenter.init(object : FakeApiService() {
            override fun getPullsFromRepo(ownerName: String, repoName: String): Call<ArrayList<PullRequest>> {
                return Calls.response(prsToReturn)
            }
        }, ownerUsername, repoName)

        presenter.loadPrs()

        Mockito.verify(view).showLoading()
        Mockito.verify(view).hideLoading()
        Mockito.verify(view).setPrs(prsToReturn)
        Mockito.verify(view).setPrsOpenClosedBalance(3, 10)
        Mockito.verifyNoMoreInteractions(view)
    }

    @Test
    fun `load lots of pull requests successfully`() {
        val prsToReturn = getNPrs(200, 110)
        presenter.init(object : FakeApiService() {
            override fun getPullsFromRepo(ownerName: String, repoName: String): Call<ArrayList<PullRequest>> {
                return Calls.response(prsToReturn)
            }
        }, ownerUsername, repoName)

        presenter.loadPrs()

        Mockito.verify(view).showLoading()
        Mockito.verify(view).hideLoading()
        Mockito.verify(view).setPrs(prsToReturn)
        Mockito.verify(view).setPrsOpenClosedBalance(200, 110)
        Mockito.verifyNoMoreInteractions(view)
    }

    @Test
    fun `load pull requests empty`() {
        val prsToReturn = getNPrs(0, 0)
        presenter.init(object : FakeApiService() {
            override fun getPullsFromRepo(ownerName: String, repoName: String): Call<ArrayList<PullRequest>> {
                return Calls.response(prsToReturn)
            }
        }, ownerUsername, repoName)

        presenter.loadPrs()

        Mockito.verify(view).showLoading()
        Mockito.verify(view).hideLoading()
        Mockito.verify(view).setPrs(prsToReturn)
        Mockito.verify(view).setPrsOpenClosedBalance(0, 0)
        Mockito.verifyNoMoreInteractions(view)
    }

    @Test
    fun `load pull requests with a server error`() {
        presenter.init(object : FakeApiService() {
            override fun getPullsFromRepo(ownerName: String, repoName: String): Call<ArrayList<PullRequest>> {
                return Calls.response(Response.error(500, ResponseBody.create(null, "")))
            }
        }, ownerUsername, repoName)

        presenter.loadPrs()

        Mockito.verify(view).showLoading()
        Mockito.verify(view).hideLoading()
        Mockito.verify(view).showError(R.string.server_error_fetching_prs)
        Mockito.verifyNoMoreInteractions(view)
    }

    @Test
    fun `load pull requests with a network error`() {
        presenter.init(object : FakeApiService() {
            override fun getPullsFromRepo(ownerName: String, repoName: String): Call<ArrayList<PullRequest>> {
                return Calls.failure(IOException())
            }
        }, ownerUsername, repoName)

        presenter.loadPrs()

        Mockito.verify(view).showLoading()
        Mockito.verify(view).hideLoading()
        Mockito.verify(view).showError(R.string.connection_error)
        Mockito.verifyNoMoreInteractions(view)
    }
}