package com.wesjon.githubpulls.screens.repositories

import com.wesjon.githubpulls.R
import com.wesjon.githubpulls.base.BasePresenterTest
import com.wesjon.githubpulls.base.FakeApiService
import com.wesjon.githubpulls.rest.model.Repository
import okhttp3.ResponseBody
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Call
import retrofit2.Response
import retrofit2.mock.Calls
import java.io.IOException


/**
 * Created by 1545 IRON V4 on 9/23/2017.
 */
@RunWith(MockitoJUnitRunner::class)
class RepositoryListPresenterTest : BasePresenterTest() {

    @Mock
    lateinit var view: RepositoryListContract.View

    private val presenter = RepositoryListPresenter()

    @Before
    fun setUp() {
        presenter.attachView(view)
    }

    @Test
    fun `load first page successfully`() {
        val successfulListToReturn = ArrayList<Repository>()
        presenter.init(object : FakeApiService() {
            override fun getJavaRepositories(page: Int): Call<ArrayList<Repository>> {
                return Calls.response(successfulListToReturn)
            }
        })

        presenter.loadNextPage()

        Mockito.verify(view).showLoadingScreen()
        Mockito.verify(view).hideLoadings()
        Mockito.verify(view).setItems(successfulListToReturn)
        Assert.assertEquals(2, presenter.currentPage)
        Mockito.verifyNoMoreInteractions(view)
    }

    @Test
    fun `load 2 or greater page successfully`() {
        val currentPage = 2
        val successfulListToReturn = ArrayList<Repository>()
        presenter.init(object : FakeApiService() {
            override fun getJavaRepositories(page: Int) = Calls.response(successfulListToReturn)
        })

        presenter.currentPage = currentPage

        presenter.loadNextPage()

        Mockito.verify(view).showLoadingNextPage()
        Mockito.verify(view).hideLoadings()
        Mockito.verify(view).setItems(successfulListToReturn)
        Assert.assertEquals(currentPage + 1, presenter.currentPage)
    }

    @Test
    fun `load page with network error`() {
        presenter.init(object : FakeApiService() {
            override fun getJavaRepositories(page: Int): Call<ArrayList<Repository>> =
                    Calls.failure(IOException("Omg, no internet"))
        })

        presenter.loadNextPage()

        Mockito.verify(view).showLoadingScreen()
        Mockito.verify(view).hideLoadings()
        Mockito.verify(view).showError(R.string.connection_error)
        Assert.assertEquals(RepositoryListPresenter.FIRST_PAGE, presenter.currentPage)
    }

    @Test
    fun `load page with a server error`() {
        presenter.init(object : FakeApiService() {
            override fun getJavaRepositories(page: Int): Call<ArrayList<Repository>> =
                    Calls.response(Response.error(500, ResponseBody.create(null, "")))
        })

        presenter.loadNextPage()

        Mockito.verify(view).showLoadingScreen()
        Mockito.verify(view).hideLoadings()
        Mockito.verify(view).showError(R.string.server_error_fetching_repos)
        Assert.assertEquals(RepositoryListPresenter.FIRST_PAGE, presenter.currentPage)
    }
}