package com.wesjon.githubpulls.base

import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by 1545 IRON V4 on 9/23/2017.
 */
@RunWith(MockitoJUnitRunner::class)
abstract class BasePresenterTest