package com.wesjon.githubpulls.base

import com.wesjon.githubpulls.rest.GithubApiService
import com.wesjon.githubpulls.rest.model.PullRequest
import com.wesjon.githubpulls.rest.model.Repository
import retrofit2.Call
import retrofit2.mock.Calls

/**
 * Created by 1545 IRON V4 on 9/23/2017.
 */
abstract class FakeApiService : GithubApiService {
    override fun getJavaRepositories(page: Int): Call<ArrayList<Repository>> {
        return Calls.response(null)
    }

    override fun getPullsFromRepo(ownerName: String, repoName: String): Call<ArrayList<PullRequest>> {
        return Calls.response(null)
    }
}